import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';

import { RoomtypeRoutingModule } from './roomtype-routing.module';
import { AddroomtypeComponent } from './addroomtype/addroomtype.component';
import { RoomtypesComponent } from './roomtypes/roomtypes.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditroomtypeComponent } from './editroomtype/editroomtype.component';

@NgModule({
  declarations: [
		AddroomtypeComponent,
		RoomtypesComponent,
		DashboardComponent,
		EditroomtypeComponent
	],
  imports: [
    CommonModule,
		RoomtypeRoutingModule,
		FormsModule,
    PrimengModule,
  ]
})
export class RoomtypeModule { }
