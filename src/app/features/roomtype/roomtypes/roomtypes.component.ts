import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

import { Basecomponent } from 'src/app/core/basecomponent';

import { Addroomtypemodel } from 'src/app/core/models/addroomtypemodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';
import { Roomtypemodel } from 'src/app/core/models/roomtypemodel';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { RoomService } from 'src/app/shared/services/room.service';

@Component({
  selector: 'app-roomtypes',
  templateUrl: './roomtypes.component.html',
  styleUrls: ['./roomtypes.component.less']
})
export class RoomtypesComponent extends Basecomponent implements OnInit {
	room = new Addroomtypemodel();
	edtDlg: boolean;

	selectedRoomType: Roomtypemodel;
	newRoomType: boolean;
	roomTypes: Roomtypemodel[];
	cols: any[];

  constructor(
		private roomSvc: RoomService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
  }

  ngOnInit() {
		this.loadRoomTypes();
		this.cols = [
			{ field: 'type', header: 'Room Type' },
			{ field: 'price', header: 'Price' },
			{ field: 'monthsAdvance', header: 'Months Advance' },
			{ field: 'monthsDeposit', header: 'Months Deposit' }
		];
	}

	updateRmType() {
		this.edtDlg = false;
		this.loadRoomTypes();
	}

  loadRoomTypes() {
    this.roomSvc
      .getRoomTypes(this.propId)
      .subscribe(
        (result: Responsemodel<Roomtypemodel[]>) => {
					if (result.data != null) this.roomTypes = result.data;
				},
        (error: HttpErrorResponse) => console.log(error.message)
      );
	}
	
	onRowSelect(evt: any) {
		this.edtDlg = true;
		this.newRoomType = false;
		this.selectedRoomType = evt.data;
	}
}
