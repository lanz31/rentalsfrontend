import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MessageService } from 'primeng/api';

import { Basecomponent } from 'src/app/core';

import { Addroomtypemodel } from 'src/app/core/models/addroomtypemodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { RoomService } from 'src/app/shared/services/room.service';

@Component({
  selector: 'app-addroomtype',
  templateUrl: './addroomtype.component.html',
  styleUrls: ['./addroomtype.component.less']
})
export class AddroomtypeComponent extends Basecomponent implements OnInit {
	@Output() onsave = new EventEmitter<boolean>();
	
  room = new Addroomtypemodel();

  constructor(
		private roomSvc: RoomService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
  }

  ngOnInit() {
  }

  addRoomType() {
    this.room.PropertyId = this.propId;
    this.roomSvc
      .addRoomType(this.room)
      .subscribe(
        (result: Responsemodel<Response>) => {
					if (result.statusCode === this.Status200) {
						this.clearfields();
						this.showMessage("success", result.message, "");

						setTimeout(() => {
							this.clearMessage();
							this.onsave.emit(true);
						}, 2000);
					} else if (result.statusCode === this.Status400) {
						this.showMessage("warn", result.message, "");
					}
        },
        (error: HttpErrorResponse) => {
					console.log(error.error.status);
					console.log(error.error.title);
					for (let key in error.error.errors) {
						console.log(key);
					}
        }
      );
  }

  clearfields() {
		this.room = new Addroomtypemodel();
  }
}
