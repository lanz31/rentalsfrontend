import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Basecomponent } from 'src/app/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent extends Basecomponent implements OnInit {
	items: MenuItem[];
	
  constructor(
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
	}

  ngOnInit() {
		this.items = [
			{
				label: 'New Room Type',
				icon: 'pi pi-plus',
				routerLink: `./new`
			},
			{ separator:true },
			{
				label: 'Room Types',
				icon: 'pi pi-bars',
				routerLink: `./list`
			}
		];
  }
}
