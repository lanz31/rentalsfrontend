import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageService } from 'primeng/api';

import { Basecomponent } from 'src/app/core/basecomponent';

import { Responsemodel } from 'src/app/core/models/responsemodel';
import { Roomtypemodel } from 'src/app/core/models/roomtypemodel';
import { Updateroomtypemodel } from 'src/app/core/models/updateroomtypemodel';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { RoomService } from 'src/app/shared/services/room.service';

@Component({
  selector: 'app-editroomtype',
  templateUrl: './editroomtype.component.html',
  styleUrls: ['./editroomtype.component.less']
})
export class EditroomtypeComponent extends Basecomponent implements OnInit {
	@Input() roomType: Roomtypemodel;
	@Output() save = new EventEmitter<boolean>();

	rmType = new Updateroomtypemodel();
	
  constructor(
		private roomSvc: RoomService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
	}

  ngOnInit() {
	}
	
	updateRoomType() {
		this.rmType.PropertyId = this.propId;
		this.rmType.RoomTypeId = this.roomType.roomTypeId;
		this.rmType.Type = this.roomType.type;
		this.rmType.Price = this.roomType.price;
		this.rmType.MonthsAdvance = this.roomType.monthsAdvance;
		this.rmType.MonthsDeposit = this.roomType.monthsDeposit;

    this.roomSvc
      .updateRoomType(this.rmType)
      .subscribe(
        (result: Responsemodel<Response>) => {
					this.clearfields();
					this.showMessage("success", result.message, "");

					setTimeout(() => {
						this.save.emit(true);
					}, 2000);
        },
        (error: HttpErrorResponse) => {
          console.log(error.message);
        }
      );
  }

  clearfields() {
		this.rmType = new Updateroomtypemodel();
		this.roomType = new Roomtypemodel();
  }
}
