import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AddroomtypeComponent } from './addroomtype/addroomtype.component';
import { RoomtypesComponent } from './roomtypes/roomtypes.component';

const routes: Routes = [
	{
		path: '',
		component: DashboardComponent,
		children: [
			{ path: 'new', component: AddroomtypeComponent },
			{ path: 'list', component: RoomtypesComponent }
		]
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomtypeRoutingModule { }
