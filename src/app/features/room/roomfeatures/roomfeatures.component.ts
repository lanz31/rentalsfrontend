import { Component, OnInit } from '@angular/core';

import { RoomService } from 'src/app/shared/services/room.service';

import { Roomfeaturesmodel } from 'src/app/core/models/roomfeaturesmodel';

@Component({
  selector: 'app-roomfeatures',
  templateUrl: './roomfeatures.component.html',
  styleUrls: ['./roomfeatures.component.less']
})
export class RoomfeaturesComponent implements OnInit {

  features: Roomfeaturesmodel[];

  propertyId: string;

  constructor(private roomService: RoomService) { }

  ngOnInit() {
    this.loadFeatures();
  }

  loadFeatures() {
    this.propertyId = localStorage.getItem('');

    // this.roomService
    //   .getRoomFeatures(this.propertyId)
    //   .subscribe(
    //     result => this.features = result,
    //     error => error
    //   );
  }

}
