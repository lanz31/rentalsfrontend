import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomfeaturesComponent } from './roomfeatures.component';

describe('RoomfeaturesComponent', () => {
  let component: RoomfeaturesComponent;
  let fixture: ComponentFixture<RoomfeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomfeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomfeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
