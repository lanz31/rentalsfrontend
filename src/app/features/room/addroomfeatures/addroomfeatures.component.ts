import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';

import { RoomService } from 'src/app/shared/services/room.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Roomlistmodel } from 'src/app/core/models/roomlistmodel';
import { Roomfeaturemodel } from 'src/app/core/models/roomfeaturemodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';

import { Basecomponent } from 'src/app/core';

@Component({
  selector: 'app-addroomfeatures',
  templateUrl: './addroomfeatures.component.html',
  styleUrls: ['./addroomfeatures.component.less']
})
export class AddroomfeaturesComponent extends Basecomponent implements OnInit {
	@Output() onsave = new EventEmitter<boolean>();
	@Input() roomId: number;

	rooms: Roomlistmodel[];
	selectedRoom: Roomlistmodel;
  roomFeature = new Roomfeaturemodel();

  constructor(
		private roomService: RoomService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
  }

  ngOnInit() {
		this.loadRooms();
  }

  loadRooms() {
    this.roomService
      .getRooms(this.propId, false)
      .subscribe(
        (result: Responsemodel<Roomlistmodel[]>) => {
					if (result.data != null) this.rooms = result.data;
				},
        (error: HttpErrorResponse) => error
      );
  }

  addFeature() {
		this.roomFeature.roomId = this.roomId;
    this.roomService
      .addRoomFeature(this.roomFeature)
      .subscribe(
        (result: Responsemodel<Response>) => {
					if (result.statusCode === this.Status200) {
						this.showMessage("success", result.message, "");
						this.clearfields(0);
						
						setTimeout(() => {
							this.onsave.emit(true);
						}, 2000);
					} else if (result.statusCode === this.Status400) {
						this.showMessage("warn", result.message, "");
						this.clearfields(1);
					}
        },
        (error: HttpErrorResponse) => console.log(error)
      );
  }

  clearfields(clrFeature: number) {
		if (clrFeature === 0) {
			this.roomFeature.name = '';
			this.selectedRoom = null;
		}
		if (clrFeature === 1) {
			this.roomFeature.name = '';
		}
  }
}
