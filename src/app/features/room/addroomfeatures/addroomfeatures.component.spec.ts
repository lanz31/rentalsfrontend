import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddroomfeaturesComponent } from './addroomfeatures.component';

describe('AddroomfeaturesComponent', () => {
  let component: AddroomfeaturesComponent;
  let fixture: ComponentFixture<AddroomfeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddroomfeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddroomfeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
