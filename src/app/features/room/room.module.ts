import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';
import { RoomRoutingModule } from './room-routing.module';

import { AddroomComponent } from './addroom/addroom.component';
import { RoominfoComponent } from './roominfo/roominfo.component';
import { RoomslistComponent } from './roomslist/roomslist.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddroomfeaturesComponent } from './addroomfeatures/addroomfeatures.component';
import { RoomfeaturesComponent } from './roomfeatures/roomfeatures.component';
import { OccupiedbedsComponent } from './occupiedbeds/occupiedbeds.component';
import { VacantbedsComponent } from './vacantbeds/vacantbeds.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PrimengModule,
    RoomRoutingModule
  ],
  declarations: [
    DashboardComponent,
    AddroomComponent,
    RoominfoComponent,
    RoomslistComponent,
    AddroomfeaturesComponent,
    RoomfeaturesComponent,
    OccupiedbedsComponent,
    VacantbedsComponent
  ],
  exports: [
    DashboardComponent,
    AddroomComponent,
    RoominfoComponent,
    RoomslistComponent,
    AddroomfeaturesComponent,
		RoomfeaturesComponent,
		OccupiedbedsComponent,
    VacantbedsComponent
  ]
})
export class RoomModule { }
