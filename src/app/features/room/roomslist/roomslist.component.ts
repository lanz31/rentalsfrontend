import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';

import { RoomService } from 'src/app/shared/services/room.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Roomlistmodel } from 'src/app/core/models/roomlistmodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';

import { Basecomponent } from 'src/app/core';
import { RoominfoComponent } from '../roominfo/roominfo.component';

@Component({
  selector: 'app-roomslist',
  templateUrl: './roomslist.component.html',
  styleUrls: ['./roomslist.component.less']
})
export class RoomslistComponent extends Basecomponent implements OnInit {
	@ViewChild(RoominfoComponent, { static: true }) roomInfo: RoominfoComponent;
	@Output() onAddFeature = new EventEmitter<string>();
	@Output() onViewInfo = new EventEmitter<string>();

	addDlg: boolean;
	addRmFeatureDlg: boolean;
	viewRmInfoDlg: boolean;
	roomId: number;

	rooms: Roomlistmodel[];
	cols: any[];

  constructor(
		private roomSvc: RoomService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
    super(httpClient, urlHelper, messageService);
  }

  ngOnInit() {
		this.loadRooms();
		this.cols = [
			{ field: 'roomName', header: 'Room Name' },
			{ field: 'totalBeds', header: 'Beds' },
			{ field: 'occupiedBeds', header: 'Occupied Beds' },
			{ field: 'roomType', header: 'Room Type' },
			{ field: 'price', header: 'Price' }
		];
  }

  loadRooms() {
    this.roomSvc
      .getRooms(this.propId, false)
      .subscribe(
        (result: Responsemodel<Roomlistmodel[]>) => {
					if (result.data != null) this.rooms = result.data;
				},
        (error: HttpErrorResponse) => console.log(error)
      );
	}

	showDialog(num: number, rid: number) {
		if (num === 0)
			this.addDlg = true;
		if (num === 1) {
			this.roomId = rid;
			this.addRmFeatureDlg = true;
		}
		if (num === 2) {
			this.roomInfo.getRoomInfo(rid);
			this.viewRmInfoDlg = true;
		}
	}

	save() {
		this.addRmFeatureDlg = false;
		this.loadRooms();
	}
}
