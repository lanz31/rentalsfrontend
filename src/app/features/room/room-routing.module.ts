import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AddroomComponent } from './addroom/addroom.component';
import { RoomslistComponent } from './roomslist/roomslist.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
			{ path: 'new', component: AddroomComponent },
			{ path: 'list', component: RoomslistComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomRoutingModule { }
