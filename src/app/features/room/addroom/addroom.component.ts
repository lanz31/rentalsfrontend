import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';

import { Basecomponent } from 'src/app/core';

import { RoomService } from 'src/app/shared/services/room.service';

import { Roomtypemodel } from 'src/app/core/models/roomtypemodel';
import { Addroommodel } from 'src/app/core/models/addroommodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';
import { Responseerrormodel } from 'src/app/core/models/responseerrormodel';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

@Component({
  selector: 'app-addroom',
  templateUrl: './addroom.component.html',
  styleUrls: ['./addroom.component.less']
})
export class AddroomComponent extends Basecomponent implements OnInit {
	@Output() onsave = new EventEmitter<boolean>();

  roomTypes: Roomtypemodel[];
  room = new Addroommodel();
  selectedRoomType: Roomtypemodel;

  constructor(
		private roomService: RoomService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
			super(httpClient, urlHelper, messageService);
  }

  ngOnInit() {
    this.loadRoomTypes();
  }

  loadRoomTypes() {
    this.roomService
      .getRoomTypes(this.propId)
      .subscribe(
        (result: Responsemodel<Roomtypemodel[]>) => {
					if (result.data != null) this.roomTypes = result.data;
				},
        (error: HttpErrorResponse) => console.log(error)
      );
  }

  addRoom() {
    this.room.PropertyId = this.propId;
    this.room.RoomTypeId = this.selectedRoomType.roomTypeId;

    this.roomService
      .addRoom(this.room)
      .subscribe(
        (result: Responsemodel<Response>) => {
					if (result.statusCode === this.Status200) {
						this.showMessage("success", result.message, "");
						this.clearfields();

						setTimeout(() => {
							this.onsave.emit(true);
						}, 2000);
					} else if (result.statusCode === this.Status204) {
						this.showMessage("warn", result.message, "");
					}
        },
        (error: HttpErrorResponse) => {
					console.log(error);
        }
      );
	}
	
	clearfields() {
		this.room = new Addroommodel();
		this.selectedRoomType = null;
  }
}
