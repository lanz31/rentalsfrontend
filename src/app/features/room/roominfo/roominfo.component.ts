import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';

import { Roominfomodel } from 'src/app/core/models/roominfomodel';

import { RoomService } from 'src/app/shared/services/room.service';

import { Basecomponent } from 'src/app/core/basecomponent';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';
import { Responsemodel } from 'src/app/core/models/responsemodel';

@Component({
  selector: 'app-roominfo',
  templateUrl: './roominfo.component.html',
  styleUrls: ['./roominfo.component.less']
})
export class RoominfoComponent extends Basecomponent implements OnInit {
	room = new Roominfomodel();

  constructor(
		private roomSvc: RoomService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
	}

  ngOnInit() {
	}

	getRoomInfo(roomId: number) {
		this.roomSvc
			.getRoomInfo(roomId)
			.subscribe(
				(result: Responsemodel<Roominfomodel>) => {
					if (result.data != null) this.room = result.data;
				},
				(error: HttpErrorResponse) => console.log(error.message)
			);
	}
}
