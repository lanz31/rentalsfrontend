import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';

import { Roomlistmodel } from 'src/app/core/models/roomlistmodel';

import { Basecomponent } from 'src/app/core/basecomponent';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';
import { RoominfoComponent } from '../roominfo/roominfo.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent extends Basecomponent implements OnInit {
	items: MenuItem[];

	addRmFeatureDlg: boolean;
	viewRmInfoDlg: boolean;
	roomId: number;

  constructor(
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
			super(httpClient, urlHelper, messageService);
		}

  ngOnInit() {
		this.items = [
			{
				label: 'New Room',
				icon: 'pi pi-plus',
				routerLink: `./new`
			},
			{ separator:true },
			{
				label: 'Rooms',
				icon: 'pi pi-bars',
				routerLink: `./list`
			}
		];
	}

	save() {
		this.addRmFeatureDlg = false;
	}
}
