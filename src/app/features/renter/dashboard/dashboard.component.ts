import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';

// Animations
import { slideInAnimation } from 'src/app/core/animation/nav-animation';

import { Basecomponent } from 'src/app/core/basecomponent';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
  animations: [slideInAnimation]
})
export class DashboardComponent extends Basecomponent implements OnInit {
	items: MenuItem[];

  constructor(
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
			super(httpClient, urlHelper, messageService);
		}

  ngOnInit() {
		this.items = [
			{
				label: 'New Renter',
				icon: 'pi pi-plus',
				routerLink: './new'
			},
			{ separator:true },
			{
				label: 'Renters',
				icon: 'pi pi-bars',
				routerLink: './list'
			}
		];
	}
}
