import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MessageService } from 'primeng/api';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { RenterService } from 'src/app/shared/services/renter.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';
import { RoomService } from 'src/app/shared/services/room.service';

import { Rentermodel } from 'src/app/core/models/rentermodel';
import { Propertiesmodel } from 'src/app/core/models/propertiesmodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';
import { Roomlistmodel } from 'src/app/core/models/roomlistmodel';
import { Checkinmodel } from 'src/app/core/models/checkinmodel';

import { Basecomponent } from 'src/app/core';

@Component({
  selector: 'app-addrenter',
  templateUrl: './addrenter.component.html',
  styleUrls: ['./addrenter.component.less'],
  animations: [
    trigger('onLoad', [
      transition(':enter', [
        style({
          transform: 'translateX(-100%)',
          opacity: 0,
        }),
        animate('1s ease-in', style({
          transform: 'translateX(0%)',
          opacity: 1
        }))
      ])
    ]),
  ]
})
export class AddrenterComponent extends Basecomponent implements OnInit {
	@Output() onSave = new EventEmitter<boolean>();
	
	renter = new Rentermodel();

	properties: Propertiesmodel[];
	rooms: Roomlistmodel[];

  constructor(
		private renterService: RenterService,
		private roomSvc: RoomService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
			super(httpClient, urlHelper, messageService);
		}

  ngOnInit() {
    this.loadRooms();
  }

  addRenter() {
		this.renter.propertyId = this.propId;
    this.renterService
      .addRenter(this.renter)
      .subscribe(
        (result: Responsemodel<Response>) => {
					if (result.statusCode === this.Status200) {
						this.clearFields();
						this.showMessage("success", result.message, "");

						setTimeout(() => {
							this.onSave.emit(true);
						}, 2000);
					} else if (result.statusCode === this.Status204) {
						this.showMessage("warn", result.message, "");
					}
        },
        (error: HttpErrorResponse) => {
          console.log(error.error.status);
					console.log(error.error.title);
					console.log(error.error);
					console.log(error);
        }
      );
	}
	
	loadRooms() {
		this.roomSvc
			.getRooms(this.propId, true)
			.subscribe(
				(result: Responsemodel<Roomlistmodel[]>) => {
					if (result.data != null) this.rooms = result.data;
				},
				(error: HttpErrorResponse) => console.log(error)
			);
	}

  clearFields() {
		this.renter = new Rentermodel();
  }

}
