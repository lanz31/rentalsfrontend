import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddrenterComponent } from './addrenter.component';

describe('AddrenterComponent', () => {
  let component: AddrenterComponent;
  let fixture: ComponentFixture<AddrenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddrenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddrenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
