import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MessageService } from 'primeng/api';

import { Basecomponent } from 'src/app/core';

import { RenterService } from 'src/app/shared/services/renter.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Rentermodel } from 'src/app/core/models/rentermodel';
import { Checkoutmodel } from 'src/app/core/models/checkoutmodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';
import { RenterinfoComponent } from '../renterinfo/renterinfo.component';

@Component({
  selector: 'app-renters',
  templateUrl: './renters.component.html',
  styleUrls: ['./renters.component.less'],
  // animations: [
  //   trigger('onLoad', [
  //     transition(':enter', [
  //       style({
  //         transform: 'translateX(-100%)',
  //         opacity: 0,
  //       }),
  //       animate('1s ease-in', style({
  //         transform: 'translateX(0%)',
  //         opacity: 1
  //       }))
  //     ])
  //   ]),
  // ]
})
export class RentersComponent extends Basecomponent implements OnInit {
	@ViewChild(RenterinfoComponent, { static: true }) renterInfoViewChild;

	checkOutModel = new Checkoutmodel();

	addRenterDlg: boolean;
	checkinDlg: boolean;
	renterInfoDlg: boolean;
	renterId: number;

	cols: any[];
  renters: Rentermodel[];
  
  constructor(
		private renterSvc: RenterService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
			super(httpClient, urlHelper, messageService);
		}

  ngOnInit() {
		this.loadRenters();
		this.cols = [
			{ field: 'name', header: 'Name' },
			{ field: 'contactNo', header: 'Contact No' },
			{ field: 'profession', header: 'Profession' }
		];
  }

	onSave(num: number) {
		if (num === 1) this.addRenterDlg = false;
		if (num === 2) this.checkinDlg = false;

		this.loadRenters();
	}
	
	showDialog(num: number, renterid: number, roomid: number) {
		if (num === 1) {
			this.renterInfoDlg = true;
			this.renterInfoViewChild.getRenterInfo(renterid);
		}
		if (num === 2) {
			this.checkinDlg = true;
			this.renterId = renterid;
		}
		if (num === 3) {
			this.deleteRenter(renterid, roomid);
		}
	}

  loadRenters() {
    this.renterSvc
      .getPropertyRenters(this.propId)
      .subscribe(
				(result: Responsemodel<Rentermodel[]>) => {
					if (result.data != null) this.renters = result.data;
				},
        (error: HttpErrorResponse) => console.log(error.message)
      );
	}
	
	deleteRenter(renterid: number, roomid: number) {
		this.checkOutModel.PropId = this.propId;
		this.checkOutModel.RoomId = roomid;
		this.checkOutModel.RenterId = renterid;

		this.renterSvc
			.checkOutRenter(this.checkOutModel)
			.subscribe(
				(response: Responsemodel<Response>) => {
					this.showMessage("success", response.message, "");
					
					setTimeout(() => {
						this.loadRenters();
					}, 2000);
				},
				(error: HttpErrorResponse) => {
					console.log(error.error.status);
					console.log(error.error.title);
					for (let key in error.error.errors) {
						console.log(key);
					}
				}
			);
	}
}
