import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from 'primeng/components/common/shared';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';
import { RenterRoutingModule } from './renter-routing.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AddrenterComponent } from './addrenter/addrenter.component';
import { RentersComponent } from './renters/renters.component';

import { RenterService } from 'src/app/shared/services/renter.service';
import { TotalrentersComponent } from './totalrenters/totalrenters.component';
import { RenterdueComponent } from './renterdue/renterdue.component';
import { RenterinfoComponent } from './renterinfo/renterinfo.component';
import { CheckinComponent } from './checkin/checkin.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PrimengModule,
    RenterRoutingModule,
    SharedModule
  ],
  declarations: [
    AddrenterComponent,
    RentersComponent,
    DashboardComponent,
    TotalrentersComponent,
    RenterdueComponent,
    RenterinfoComponent,
    CheckinComponent
  ],
  exports: [
    AddrenterComponent,
    RentersComponent,
		DashboardComponent,
		TotalrentersComponent,
		RenterdueComponent,
		CheckinComponent
  ],
  providers: [RenterService]
})
export class RenterModule { }
