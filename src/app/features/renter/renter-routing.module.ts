import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AddrenterComponent } from './addrenter/addrenter.component';
import { RentersComponent } from './renters/renters.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
			{ path: 'new', component: AddrenterComponent },
			{ path: 'list', component: RentersComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RenterRoutingModule { }
