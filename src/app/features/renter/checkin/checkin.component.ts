import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageService } from 'primeng/api';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';
import { RoomService } from 'src/app/shared/services/room.service';
import { RenterService } from 'src/app/shared/services/renter.service';

import { Checkinmodel } from 'src/app/core/models/checkinmodel';
import { Roomlistmodel } from 'src/app/core/models/roomlistmodel';
import { Rentermodel } from 'src/app/core/models/rentermodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';

import { Basecomponent } from 'src/app/core';

@Component({
  selector: 'app-checkin',
  templateUrl: './checkin.component.html',
  styleUrls: ['./checkin.component.less']
})
export class CheckinComponent extends Basecomponent implements OnInit {
	@Input() renterId: number;
	@Output() onSave = new EventEmitter<boolean>();
	
	checkin = new Checkinmodel();

	selectedRoom: Roomlistmodel;

	rooms: Roomlistmodel[];
	renters: Rentermodel[];

  constructor(
		private roomSvc: RoomService,
		private renterSvc: RenterService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
			super(httpClient, urlHelper, messageService);
		}

  ngOnInit() {
		this.loadRooms();
		this.loadRenters();
	}
	
	checkinRenter() {
		this.checkin.PropId = this.propId;
		this.checkin.RoomId = this.selectedRoom.roomId;
		this.checkin.RenterId = this.renterId;

		this.renterSvc
			.checkInRenter(this.checkin)
			.subscribe(
				(result: Responsemodel<Response>) => {
					if (result.statusCode === this.Status200) {
						this.clearFields();
						this.showMessage("success", result.message, "");

						setTimeout(() => {
							this.onSave.emit(true);
						}, 2000);
					} else if (result.statusCode === this.Status400) {
						this.showMessage("warn", result.message, "");
					}
				},
				(error: HttpErrorResponse) => {
					console.log(error);
        }
			);
	}

	loadRooms() {
		this.roomSvc
			.getRooms(this.propId, true)
			.subscribe(
				(result: Responsemodel<Roomlistmodel[]>) => {
					if (result.data != null) this.rooms = result.data;
				},
				(error: HttpErrorResponse) => console.log(error)
			);
	}

	loadRenters() {
		this.renterSvc
			.getPropertyRenters(this.propId)
			.subscribe(
				(result: Responsemodel<Rentermodel[]>) => {
					if (result.data != null) this.renters = result.data;
				},
				(error: HttpErrorResponse) => console.log(error)
			);
	}

	clearFields() {
		this.checkin = new Checkinmodel();
		this.selectedRoom = null;
		this.renterId = 0;
	}
}
