import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

import { RenterService } from 'src/app/shared/services/renter.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Rentermodel } from 'src/app/core/models/rentermodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';

import { Basecomponent } from 'src/app/core/basecomponent';

@Component({
  selector: 'app-renterinfo',
  templateUrl: './renterinfo.component.html',
  styleUrls: ['./renterinfo.component.less']
})
export class RenterinfoComponent extends Basecomponent implements OnInit {
	renter = new Rentermodel();

  constructor(
		private renterService: RenterService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
			super(httpClient, urlHelper, messageService);
		}

  ngOnInit() {
	}
	
	getRenterInfo(renterId: number) {
		this.renterService
			.getRenterInfo(renterId)
			.subscribe(
				(result: Responsemodel<Rentermodel>) => {
					if (result.data != null) this.renter = result.data;
				},
				(error: HttpErrorResponse) => console.log(error.message)
			);
	}
}
