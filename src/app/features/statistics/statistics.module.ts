import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';

import { StatisticComponent } from './statistic/statistic.component';

@NgModule({
  imports: [
    CommonModule,
    PrimengModule
  ],
  declarations: [StatisticComponent],
  exports: [StatisticComponent]
})
export class StatisticsModule { }
