import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Basecomponent } from 'src/app/core/basecomponent';

@Component({
  selector: 'app-newinvoice',
  templateUrl: './newinvoice.component.html',
  styleUrls: ['./newinvoice.component.less']
})
export class NewinvoiceComponent extends Basecomponent implements OnInit {	
	cols: any[];

  constructor(
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
			super(httpClient, urlHelper, messageService);
		}

  ngOnInit() {
		this.cols = [
			{ field: '', header: 'Invoice Name' },
			{ field: '', header: 'Date Created' }
		];
	}
}
