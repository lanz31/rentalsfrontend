import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';
import { InvoicesRoutingModule } from './invoices-routing.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { NewinvoiceComponent } from './newinvoice/newinvoice.component';

@NgModule({
  declarations: [
		DashboardComponent,
		NewinvoiceComponent
	],
  imports: [
    CommonModule,
		InvoicesRoutingModule,
		FormsModule,
		PrimengModule
  ]
})
export class InvoicesModule { }
