import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';

import { TransactionsRoutingModule } from './transactions-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
		TransactionsRoutingModule,
		FormsModule,
    PrimengModule
  ]
})
export class TransactionsModule { }
