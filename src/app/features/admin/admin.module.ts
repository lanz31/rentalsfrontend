import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Custom Modules
import { CommoncomponentsModule } from 'src/app/commoncomponents/commoncomponents.module';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';
import { PropertyModule } from '../property/property.module';
import { RoomModule } from '../room/room.module';
import { RenterModule } from '../renter/renter.module';
import { StatisticsModule } from '../statistics/statistics.module';

import { AdminRoutingModule } from './admin-routing.module';

import { MainComponent } from './main/main.component';
import { PropertydetailComponent } from './propertydetail/propertydetail.component';
import { RenterlistComponent } from './renterlist/renterlist.component';

import { RoomlistComponent } from './roomlist/roomlist.component';
import { RoomtypeComponent } from './roomtype/roomtype.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    PrimengModule,
    PropertyModule,
    StatisticsModule,
    RoomModule,
    RenterModule,
    CommoncomponentsModule
  ],
  declarations: [
    MainComponent,
    PropertydetailComponent,
    RoomlistComponent,
    RoomtypeComponent,
    RenterlistComponent
  ]
})
export class AdminModule { }
