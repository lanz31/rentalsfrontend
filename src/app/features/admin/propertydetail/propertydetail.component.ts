import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { HttpClient } from '@angular/common/http';

import { PropertyService } from 'src/app/shared/services/property.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Propertiesmodel } from 'src/app/core/models/propertiesmodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';
import { Propertyinfomodel } from 'src/app/core/models/propertyinfomodel';

import { Basecomponent } from 'src/app/core';

@Component({
  selector: 'app-propertydetail',
  templateUrl: './propertydetail.component.html',
  styleUrls: ['./propertydetail.component.less']
})
export class PropertydetailComponent extends Basecomponent implements OnInit {
  propertyImg: string;
  propertyModel: Propertiesmodel;

  items: MenuItem[];
  activeItem: MenuItem;

  constructor(
    private propertyService: PropertyService,
		private activatedRouter: ActivatedRoute,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService
    ) {
      super(httpClient, urlHelper, messageService);
      this.propertyImg = 'assets/img/house1.jpg';
    }

  ngOnInit() {
    this.activatedRouter.params.subscribe(params => {
      this.propId = parseInt(params['id']);

      this.loadPropertyDetails(this.propId);
    });

    this.items = [
      { label: 'Details', routerLink: './detail' },
      { label: 'Rooms', routerLink: './rooms' },
      { label: 'Renters', routerLink: '' }
    ];

    this.activeItem = this.items[0];
  }

  loadPropertyDetails(propertyId: number) {
    this.propertyService
      .getPropertyInfo(propertyId)
      .subscribe(
        (result: Responsemodel<Propertyinfomodel>) => this.propertyModel = result.data,
        error => console.log(error)
      );
  }

}
