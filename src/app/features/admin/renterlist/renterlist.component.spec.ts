import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenterlistComponent } from './renterlist.component';

describe('RenterlistComponent', () => {
  let component: RenterlistComponent;
  let fixture: ComponentFixture<RenterlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenterlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenterlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
