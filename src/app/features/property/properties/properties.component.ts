import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Message, MessageService } from 'primeng/api';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { PropertyService } from '../../../shared/services/property.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Propertiesmodel } from 'src/app/core/models/propertiesmodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';

import { Basecomponent } from 'src/app/core/basecomponent';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.less'],
  // animations: [
  //   trigger('onLoad', [
  //     transition(':enter', [
  //       style({
  //         transform: 'translateX(-100%)',
  //         opacity: 0,
  //       }),
  //       animate('1s ease-in', style({
  //         transform: 'translateX(0%)',
  //         opacity: 1
  //       }))
  //     ])
  //   ]),
  // ]
})
export class PropertiesComponent extends Basecomponent implements OnInit {
	@Output() onSelectProperty = new EventEmitter<Propertiesmodel>();

  properties: Propertiesmodel[];
	msgs: Message[] = [];

  constructor(
		private propertySvc: PropertyService,
		private router: Router,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
	}

  ngOnInit() {
    this.loadProperties();
  }

  loadProperties() {
    this.propertySvc
    .getProperties(this.currUser.userId)
    .subscribe(
      (result: Responsemodel<Propertiesmodel[]>) => {
				if (result.data != null) this.properties = result.data;
			},
      (error: HttpErrorResponse) => console.log('Error: ' + error)
    );
  }

  onNewProperty() {
		this.router.navigateByUrl('/property/add');
	}
	
	refresh() {
		this.loadProperties();
	}

  // onSelectedProperty(prop: Propertiesmodel) {
  //   this.onSelectProperty.emit(prop);
  // }

  // onNavigateToComponent(component: string, propertyId: string) {
  //   this.SelectModules.emit({component, propertyId});
  // }

  // addTerms() {
  //   this.propTerms.propertyId = this.selectedProp.id;
  //   this.propertyService
  //     .addPropertyTerms(this.propTerms)
  //     .subscribe(
  //       result => {
  //         this.msgs = [];
  //         this.msgs.push({severity: 'success', detail: result.message});

  //         this.propTerms.monthDeposit = 0;
  //         this.propTerms.monthAdvance = 0;
  //       },
  //       error => {
  //         this.msgs.push({severity: 'error', detail: 'An error occurred while adding property terms.' });
  //         console.log("Error: " + error.message)
  //       }
  //     );
  // }

  // updatePropertyInfo() {
  //   this.uptPropertyModel = new Updatepropertymodel();
  //   this.uptPropertyModel.propertyId = this.selectedProp.id;
  //   this.uptPropertyModel.userId = this.selectedProp.userId;
  //   this.uptPropertyModel.name = this.selectedProp.name;
  //   this.uptPropertyModel.address = this.selectedProp.address;
  //   this.uptPropertyModel.city = this.selectedProp.city;
  //   this.uptPropertyModel.contactNo = this.selectedProp.contactNo;
  //   this.uptPropertyModel.owner = this.selectedProp.owner;
  //   this.uptPropertyModel.totalRooms = this.selectedProp.totalRooms;

  //   this.propertyService
  //     .updatePropertyInfo(this.uptPropertyModel)
  //     .subscribe(
  //       result => {
  //         this.msgs = [];
  //         this.msgs.push({severity: 'success', detail: result.message});
  //       },
  //       error => {
  //         this.msgs.push({severity: 'error', detail: 'An error occurred while updating property info.' });
  //         console.log("Error: " + error.message)
  //       }
  //     );
  // }

  // onDialogHide() {
  //   this.selectedProp = null;
  // }

}
