import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MessageService } from 'primeng/components/common/api';

// Custom Modules
import { PropertyRoutingModule } from './property-routing.module';
import { CommoncomponentsModule } from 'src/app/commoncomponents/commoncomponents.module';
import { PrimengModule } from 'src/app/shared/primeng/primeng.module';
import { RenterModule } from '../renter/renter.module';
import { RoomModule } from '../room/room.module';
import { InvoicesModule } from '../invoices/invoices.module';
import { TransactionsModule } from '../transactions/transactions.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AddpropertyComponent } from './addproperty/addproperty.component';
import { PropertiesComponent } from './properties/properties.component';

import { PropertyService } from 'src/app/shared/services/property.service';
import { SummaryComponent } from './summary/summary.component';
import { PropertyinfoComponent } from './propertyinfo/propertyinfo.component';
// import { TestdirectiveDirective } from './testdirective.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PrimengModule,
    PropertyRoutingModule,
		CommoncomponentsModule,
		RenterModule,
		RoomModule,
		InvoicesModule,
		TransactionsModule
  ],
  declarations: [
    AddpropertyComponent,
    PropertiesComponent,
    DashboardComponent,
    SummaryComponent,
    PropertyinfoComponent
  ],
  exports: [
    AddpropertyComponent,
    PropertiesComponent,
    DashboardComponent,
		SummaryComponent
  ],
  providers: [PropertyService, MessageService]
})
export class PropertyModule { }
