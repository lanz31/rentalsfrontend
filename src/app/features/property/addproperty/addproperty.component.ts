import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';

import { PropertyService } from 'src/app/shared/services/property.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Addpropertymodel } from 'src/app/core/models/addpropertymodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';
import { Addpropertyerrormodel } from 'src/app/core/models/addpropertyerrormodel';

import { Basecomponent } from 'src/app/core/basecomponent';

@Component({
  selector: 'app-addproperty',
  templateUrl: './addproperty.component.html',
  styleUrls: ['./addproperty.component.less'],
  // animations: [
  //   trigger('onLoad', [
  //     transition(':enter', [
  //       style({
  //         transform: 'translateX(-100%)',
  //         opacity: 0,
  //       }),
  //       animate('1s ease-in', style({
  //         transform: 'translateX(0%)',
  //         opacity: 1
  //       }))
  //     ])
  //   ]),
  // ]
})
export class AddpropertyComponent extends Basecomponent implements OnInit {
	model = new Addpropertymodel();
	err: Addpropertyerrormodel;
	
  constructor(
		private propertyService: PropertyService,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
    super(httpClient, urlHelper, messageService);
  }

  ngOnInit() {
  }

  addProperty() {
    this.model.UserId = this.currUser.userId;
		this.propertyService
      .addProperty(this.model)
      .subscribe(
        (result: Responsemodel<Response>) => {
					if (result.statusCode === this.Status200) {
						this.showMessage("success", result.message, "");
						this.clearfields();
					} else if (result.statusCode === this.Status204) {
						this.showMessage("warn", result.message, "");
					}
        },
        (error: HttpErrorResponse) => {
					this.err = error.error.errors;
					console.log(this.err);
        }
      );
  }

  clearfields() {
		this.model = new Addpropertymodel();
  }
}
