import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';

import { PropertyService } from 'src/app/shared/services/property.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Propertyinfomodel } from 'src/app/core/models/propertyinfomodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';

import { Basecomponent } from 'src/app/core/basecomponent';

@Component({
  selector: 'app-propertyinfo',
  templateUrl: './propertyinfo.component.html',
  styleUrls: ['./propertyinfo.component.less']
})
export class PropertyinfoComponent extends Basecomponent implements OnInit {
	propInfo: Propertyinfomodel;
	image: string = "./assets/img/house7.jpg";

  constructor(
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService,
		private propertySvc: PropertyService) {
			super(httpClient, urlHelper, messageService);
		}

  ngOnInit() {
		this.loadPropertyInfo();
	}
	
	loadPropertyInfo() {
		this.propertySvc
			.getPropertyInfo(this.propId)
			.subscribe(
				(result: Responsemodel<Propertyinfomodel>) => this.propInfo = result.data,
				(error: HttpErrorResponse) => console.log(error.message)
			);
	}
}
