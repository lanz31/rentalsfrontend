import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { MessageService } from 'primeng/components/common/api';

import { Basecomponent } from 'src/app/core/basecomponent';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
  // animations: [slideInAnimation]
})
export class DashboardComponent extends Basecomponent implements OnInit {

	items: MenuItem[];

  constructor(
		private actvRoute: ActivatedRoute,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
	}

  ngOnInit() {
		this.actvRoute.params.subscribe(params => {
			localStorage.removeItem('propId');
			localStorage.setItem('propId', params['id']);
    });

		this.items = [
      {
        label: 'Summary',
        icon: 'pi pi-pw pi-file',
        routerLink: './summary'
      },
      {
        label: 'Room Types',
        icon: 'pi pi-fw pi-pencil',
        routerLink: './roomtypes'
      },
      {
        label: 'Rooms',
        icon: 'pi pi-fw pi-question',
        routerLink: './rooms'
			},
      {
        label: 'Renters',
        icon: 'pi pi-fw pi-question',
        routerLink: './renters'
			},
			{
        label: 'Invoices',
        icon: 'pi pi-fw pi-question',
        routerLink: './invoices'
      },
			{
        label: 'Transactions',
        icon: 'pi pi-fw pi-question',
        routerLink: './transactions'
			},
      {
        label: 'Settings',
        icon: 'pi pi-fw pi-pencil',
        routerLink: './settings'
      }
    ];
	}

}
