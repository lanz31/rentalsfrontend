import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PropertiesComponent } from './properties/properties.component';
import { AddpropertyComponent } from './addproperty/addproperty.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  // {
    // path: '',
    // component: PropertiesComponent,
		// children: [
		// 	{ path: 'add', component: AddpropertyComponent, pathMatch: 'full' },
		// 	{
		// 		path: ':id',
		// 		component: DashboardComponent,
		// 		children: [
		// 			{ path: 'summary', component: SummaryComponent },
		// 			{ path: 'roomtypes', loadChildren: () => import('../roomtype/roomtype.module').then(m => m.RoomtypeModule) },
		// 			{ path: 'rooms', loadChildren: () => import('../room/room.module').then(m => m.RoomModule) },
		// 			{ path: 'renters', loadChildren: () => import('../renter/renter.module').then(m => m.RenterModule) },
		// 			{ path: 'invoices', loadChildren: () => import('../invoices/invoices.module').then(m => m.InvoicesModule) },
		// 			{ path: 'transactions', loadChildren: () => import('../transactions/transactions.module').then(m => m.TransactionsModule) },
		// 			{ path: 'settings', loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule) }
		// 		]
		// 	}
    // ]
		// children: [
		// 	{ path: 'add', component: AddpropertyComponent },
		// 	{ path: ':id/info', component: PropertydetailsComponent }
    // ]
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertyRoutingModule { }
