export class Apiurlhelper {
	getApiUrl() {
		return 'http://localhost:62568/';
		// return "http://devapi.somee.com/";
	}

	clientApp() {
		return 'webapp';
	}

	tokenApiUrl: string = "api/v1/account/login";

	// Properties
	propertyApiUrl: string = "api/v1/property";
	getPropertyInfoApiUrl: string = "api/v1/Property/info";
	updatePropertyInfo: string = "api/v1/Property";

	// Rooms/Room Types/Room Features
	roomApiUrl: string = "api/v1/Room";
	updateRoomTypeApiUrl: string = "api/v1/Room/type";
	
	// Renters
	renterApiUrl: string = "api/v1/Renter";
}