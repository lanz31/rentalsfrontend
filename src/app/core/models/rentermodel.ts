export class Rentermodel {
	id: number;
	name: string;
	checkIn: any;
	email: string;
	contactNo: string;
	address: string;
	profession: string;
	propertyId: number;
	roomId: number;
}