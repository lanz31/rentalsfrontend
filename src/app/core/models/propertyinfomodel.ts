export class Propertyinfomodel {
	id: number;
	userId: string;
	name: string;
	address: string;
	city: string;
	contactNo: string;
	totalRooms: number;
}
