export class Roomtypemodel {
    roomTypeId: number;
    type: string;
		price: number;
		monthsAdvance: number;
		monthsDeposit: number;
    propertyId: number;
}
