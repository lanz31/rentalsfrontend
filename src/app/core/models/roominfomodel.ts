export class Roominfomodel {
	roomId: number;
	roomName: string;
	roomTypeName: string;
	totalBeds: number;
	occupiedBeds: number;
	price: number;
	monthsAdvance: number;
	monthsDeposit: number;
	features: string[];
}
