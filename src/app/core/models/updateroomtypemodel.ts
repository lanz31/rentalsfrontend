export class Updateroomtypemodel {
	RoomTypeId: number;
	Type: string;
	Price: number;
	MonthsAdvance: number;
	MonthsDeposit: number;
	PropertyId: number;
}
