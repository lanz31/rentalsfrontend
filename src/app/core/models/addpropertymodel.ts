export class Addpropertymodel {
    UserId: number;
    Name: string;
    Address: string;
    City: string;
    ContactNo: string;
    TotalRooms: number;
}
