export class Updatepropertymodel {
    propertyId: number;
    userId: number;
    name: string;
    address: string;
    city: string;
    contactNo: string;
    owner: string;
    totalRooms: number;
}
