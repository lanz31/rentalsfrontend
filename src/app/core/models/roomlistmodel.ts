export class Roomlistmodel {
	roomId: number;
	roomName: string;
	totalBeds: number;
	occupiedBeds: number;
	roomType: string;
	price: number;
	monthsAdvance: number;
	monthsDeposit: number;
}
