export class Responseerrormodel {
	type: string;
	title: string;
	status: number;
	traceId: string;
	errors: ResponseErrors;
}

export class ResponseErrors {
	error: ErrorMessage[];
}

export class ErrorMessage {
	msg: string[]
}
