export class Propertiesmodel {
	id: number;
	name: string;
	contactNo: string;
	address: string;
	city: string;
	totalRooms: number;
}
