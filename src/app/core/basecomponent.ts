import { HttpClient } from '@angular/common/http';
import { MessageService } from 'primeng/components/common/messageservice';

import { Apiurlhelper } from '../helpers/apiurlhelper';

export class Basecomponent {
	protected currUser: any;
	protected userId: any;
	protected propId: number;
	
	public Status200: number = 200;
	public Status204: number = 204;
	public Status400: number = 400;

	constructor(
		protected httpClient: HttpClient,
		protected urlHelper: Apiurlhelper,
		protected messageService: MessageService) {
		this.currUser = JSON.parse(localStorage.getItem('currentUser'));
		this.propId = parseInt(localStorage.getItem('propId'));
	}

	showMessage(severity: string, summary: string, detail: string) {
		this.messageService.add({severity: severity, summary: summary, detail: detail});
	}

	clearMessage() {
		this.messageService.clear();
	}
}
