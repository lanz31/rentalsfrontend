import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MessageService } from 'primeng/components/common/api';

import { AppComponent } from './app.component';

import { Apiurlhelper } from './helpers/apiurlhelper';

import { CommoncomponentsModule } from './commoncomponents/commoncomponents.module';

import { SessionService } from './shared/services/session.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    CommoncomponentsModule
  ],
  providers: [
    Apiurlhelper,
		SessionService,
		MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
