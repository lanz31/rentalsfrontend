import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/api';

import { Basecomponent } from 'src/app/core/basecomponent';

import { Tokenmodel } from 'src/app/core/models/tokenmodel';
import { Usermodel } from 'src/app/core/models/usermodel';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent extends Basecomponent implements OnInit {
	spinnerHidden: boolean;
	user = new Usermodel();

  constructor(
    private authService: AuthenticationService,
		private router: Router,
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService
  ) { 
		super(httpClient, urlHelper, messageService);
	}

  ngOnInit() {
		this.focusElement();
		this.spinnerHidden = true;
  }

  login() {
		// this.router.navigateByUrl('property')
		this.spinnerHidden = false;
    this.authService
      .authenticateUser(this.user)
      .subscribe(
        (result: Tokenmodel) => {
					if (result === null) {
						this.spinnerHidden = true;
						this.showMessage("error", "Invalid login.", "");
					} else {
						this.spinnerHidden = true;
						
						setTimeout(() => {
							this.router.navigateByUrl('property');
						}, 1000);
					}
				},
        (error: HttpErrorResponse) => console.log("Error: " + error.message)
      );
  }

  focusElement() {
    this.user.Email = 'admin@gmail.com';
    this.user.Password = 'Demo1231!';
  }

}
