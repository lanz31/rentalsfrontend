import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PrimengModule } from '../shared/primeng/primeng.module';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

@NgModule({
	declarations: [
		LoginComponent,
		HeaderComponent,
		SidebarComponent,
		DashboardComponent,
		PagenotfoundComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		PrimengModule
	],
	exports: [
		HeaderComponent,
		SidebarComponent,
		LoginComponent
	]
})
export class CommoncomponentsModule { }