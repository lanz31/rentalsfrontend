import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// 3rd party components
import { MenuItem } from 'primeng/api';

import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit {

  items: MenuItem[];

  constructor(
    private authService: AuthenticationService,
    private router: Router) { }

  ngOnInit() {
    this.items = [
      {
        label: 'Dashboard',
        icon: 'pi pi-pw pi-file',
        routerLink: ''
      },
      {
        label: 'Properties',
        icon: 'pi pi-fw pi-pencil',
        routerLink: './property'
      },
      {
        label: 'Room',
        icon: 'pi pi-fw pi-question',
        routerLink: './room'
			},
      {
        label: 'Renter',
        icon: 'pi pi-fw pi-question',
        routerLink: './renter'
      },
      {
        label: 'Settings',
        icon: 'pi pi-fw pi-pencil',
        routerLink: './settings'
      }
    ];
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('');
  }

}
