import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Tokenmodel } from '../../core/models/tokenmodel';
import { Usermodel } from '../../core/models/usermodel';

import { Apiurlhelper } from '../../helpers/apiurlhelper';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private httpClient: HttpClient,
    private urlHelper: Apiurlhelper) {
  }

  authenticateUser(user: Usermodel) : Observable<Tokenmodel> {
    let headers = new HttpHeaders({
      'Content-type': 'application/json'
    });

    return this.httpClient
      .post(
        this.urlHelper.getApiUrl() + this.urlHelper.tokenApiUrl,
        JSON.stringify(user),
        { headers: headers }
      )
      .pipe(
        map((result: Tokenmodel) => {
          // login successful if there's a jwt token in the response
          if (result && result.accessToken) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(result));
            localStorage.setItem('token', result.accessToken);
          }
  
          return result;
        })
      );
  }

  isLoggedIn() {
    let token = localStorage.getItem('token');
    if (token !== undefined || token !== '') {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    // localStorage.removeItem('token');
    // localStorage.removeItem('userid');
    localStorage.clear();
  }

}