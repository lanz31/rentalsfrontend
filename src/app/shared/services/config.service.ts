import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private httpClient: HttpClient) { }

  public getApiUrl() : Observable<string> {
    return this.httpClient
      .get('./assets/config/appsettings.json')
      .pipe(
        map((res: string[]) => res[0])
      );
  }

}