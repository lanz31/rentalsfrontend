import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/components/common/messageservice';

import { Basecomponent } from 'src/app/core';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

@Injectable({
  providedIn: 'root'
})
export class BaseService extends Basecomponent {
	constructor(
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
	}
}
