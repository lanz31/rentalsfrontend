import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

import { BaseService } from './base.service';
import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Responsemodel } from 'src/app/core/models/responsemodel';
import { Addroomtypemodel } from 'src/app/core/models/addroomtypemodel';
import { Roomtypemodel } from 'src/app/core/models/roomtypemodel';
import { Roomfeaturemodel } from 'src/app/core/models/roomfeaturemodel';
import { Addroommodel } from 'src/app/core/models/addroommodel';
import { Roomlistmodel } from 'src/app/core/models/roomlistmodel';
import { Updateroomtypemodel } from 'src/app/core/models/updateroomtypemodel';
import { Roominfomodel } from 'src/app/core/models/roominfomodel';

@Injectable({
  providedIn: 'root'
})
export class RoomService extends BaseService {
  constructor(
    httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
	}
	
	addRoomType(roomtype: Addroomtypemodel) {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .post(
        this.urlHelper.getApiUrl() + this.urlHelper.roomApiUrl + "/type",
        JSON.stringify(roomtype),
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Response>) => result)
      );
	}
	
	getRoomTypes(propertyid: number) {
    let headers = new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .get(
				this.urlHelper.getApiUrl() + this.urlHelper.roomApiUrl + "/" + propertyid + "/types",
				{ headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Roomtypemodel[]>) => result)
      );
	}

	updateRoomType(roomType: Updateroomtypemodel) {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .put(
				this.urlHelper.getApiUrl() + this.urlHelper.updateRoomTypeApiUrl,
				JSON.stringify(roomType),
				{ headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Response>) => result)
      );
	}
	
	addRoom(room: Addroommodel) {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .post(
        this.urlHelper.getApiUrl() + this.urlHelper.roomApiUrl,
        JSON.stringify(room),
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Response>) => { return result } )
      );
	}
	
	getRooms(propertyid: number, filter: boolean) {
    let headers = new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .get(
				this.urlHelper.getApiUrl() + this.urlHelper.roomApiUrl + "/" + propertyid + "/list?filter=" + filter,
				{ headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Roomlistmodel[]>) => result)
      );
	}

  addRoomFeature(roomfeature: Roomfeaturemodel) {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .post(
        this.urlHelper.getApiUrl() + this.urlHelper.roomApiUrl + "/features/add",
        JSON.stringify(roomfeature),
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Response>) => result)
      );
	}
	
	getRoomInfo(roomId: number) {
    let headers = new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .get(
        this.urlHelper.getApiUrl() + this.urlHelper.roomApiUrl + "/" + this.propId + "/info/" + roomId,
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Roominfomodel>) => { return result } )
      );
  }
}
