import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

import { BaseService } from './base.service';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Rentermodel } from 'src/app/core/models/rentermodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';
import { Checkinmodel } from 'src/app/core/models/checkinmodel';
import { Checkoutmodel } from 'src/app/core/models/checkoutmodel';

@Injectable({
  providedIn: 'root'
})
export class RenterService extends BaseService {
  constructor(
    httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
		super(httpClient, urlHelper, messageService);
	}

  addRenter(renter: Rentermodel) {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .post(
        this.urlHelper.getApiUrl() + this.urlHelper.renterApiUrl + "/add",
        JSON.stringify(renter),
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Response>) => result)
      );
  }

  getPropertyRenters(propertyid: number) {
    let headers = new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .get(
        this.urlHelper.getApiUrl() + this.urlHelper.renterApiUrl + "/" + propertyid + "/list",
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Rentermodel[]>) => result)
      );
	}
	
	getRenterInfo(rid: number) {
    let headers = new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .get(
        this.urlHelper.getApiUrl() + this.urlHelper.renterApiUrl + "/" + this.propId + "/info/" + rid,
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Rentermodel>) => result)
      );
	}
	
	checkInRenter(renter: Checkinmodel) {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .post(
        this.urlHelper.getApiUrl() + this.urlHelper.renterApiUrl + "/checkin",
        JSON.stringify(renter),
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Response>) => result)
      );
	}
	
	checkOutRenter(renter: Checkoutmodel) {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .put(
        this.urlHelper.getApiUrl() + this.urlHelper.renterApiUrl + "/checkout",
        JSON.stringify(renter),
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Response>) => result)
      );
  }
}
