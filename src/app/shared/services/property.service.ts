import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MessageService } from 'primeng/components/common/messageservice';

import { BaseService } from './base.service';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

import { Propertiesmodel } from 'src/app/core/models/propertiesmodel';
import { Responsemodel } from 'src/app/core/models/responsemodel';
import { Updatepropertymodel } from 'src/app/core/models/updatepropertymodel';
import { Addpropertymodel } from 'src/app/core/models/addpropertymodel';
import { Propertyinfomodel } from 'src/app/core/models/propertyinfomodel';

@Injectable({
  providedIn: 'root'
})
export class PropertyService extends BaseService {
  constructor(
		httpClient: HttpClient,
		urlHelper: Apiurlhelper,
		messageService: MessageService) {
			super(httpClient, urlHelper, messageService);
		}

  /* PROPERTIES */
  public addProperty(property: Addpropertymodel) {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .post(
        this.urlHelper.getApiUrl() + this.urlHelper.propertyApiUrl + "/add",
        JSON.stringify(property),
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Response>) => result)
      );
  }

  public getProperties(uid: number) {
    let headers = new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'bearer ' + this.currUser.accessToken
    });

    return this.httpClient
      .get(
        this.urlHelper.getApiUrl() + this.urlHelper.propertyApiUrl + "/" + uid,
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Propertiesmodel[]>) => result)
      );
  }

  public getPropertyInfo(pid: number) {
    let headers = new HttpHeaders({
      'Accept': 'application/json',
      'Authorization': 'bearer ' + localStorage.getItem('token')
    });

    return this.httpClient
      .get(
				this.urlHelper.getApiUrl() + this.urlHelper.propertyApiUrl + "/" + pid + "/info",
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Propertyinfomodel>) => result)
      );
  }

  public updatePropertyInfo(property: Updatepropertymodel) {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': 'bearer ' + localStorage.getItem('token')
    });

    return this.httpClient
      .put(
        this.urlHelper.getApiUrl() + this.urlHelper.updatePropertyInfo,
        JSON.stringify(property),
        { headers: headers }
      )
      .pipe(
        map((result: Responsemodel<Response>) => { return result } )
      );
  }
  /* END */
}