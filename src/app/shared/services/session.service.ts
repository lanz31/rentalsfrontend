import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Apiurlhelper } from 'src/app/helpers/apiurlhelper';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  protected currUser: any;

  constructor(protected urlHelper: Apiurlhelper) {
      this.currUser = JSON.parse(localStorage.getItem('currentUser'));
  }
}
