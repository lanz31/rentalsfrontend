import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TieredMenuModule } from 'primeng/tieredmenu';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { PanelMenuModule } from 'primeng/panelmenu';
import { PanelModule } from 'primeng/panel';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DataViewModule } from 'primeng/dataview';
import { AccordionModule } from 'primeng/accordion';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { SidebarModule } from 'primeng/sidebar';
import { ChartModule } from 'primeng/chart';
import { TabMenuModule } from 'primeng/tabmenu';
import { ToastModule } from 'primeng/toast';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { TooltipModule } from 'primeng/tooltip';
import { MenubarModule } from 'primeng/menubar';

@NgModule({
  imports: [
    CommonModule,
    TieredMenuModule,
    InputTextModule,
    ButtonModule,
    PanelMenuModule,
    MessagesModule,
    MessageModule,
    DataViewModule,
    PanelModule,
    AccordionModule,
    DropdownModule,
    DialogModule,
    TableModule,
    CardModule,
    SidebarModule,
    ChartModule,
		TabMenuModule,
		ToastModule,
		ProgressSpinnerModule,
		TooltipModule,
		MenubarModule
  ],
  exports: [
    TieredMenuModule,
    InputTextModule,
    ButtonModule,
    PanelMenuModule,
    MessagesModule,
    MessageModule,
    DataViewModule,
    PanelModule,
    AccordionModule,
    DropdownModule,
    DialogModule,
    TableModule,
    CardModule,
    SidebarModule,
    ChartModule,
		TabMenuModule,
		ToastModule,
		ProgressSpinnerModule,
		TooltipModule,
		MenubarModule
  ],
  declarations: []
})
export class PrimengModule { }
